var express = require('express');
require('dotenv').config()
var app = express();
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var date = require('date-and-time');
var bcrypt = require('bcrypt');

var baseMlabURL = process.env.URLmlab;
var mlabAPIKey = process.env.APIKEYmlab;

app.use(bodyParser.json());
var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1',
    function(req, res) {
        console.log("GET /apitechu/v1");
          res.send(
          {
            "msg":"Bienvenido a mi API"
          }
        )
    }
);

app.get('/apitechu/v1/users',
function(req, res)
{
  console.log("GET /apitechu/v1/users");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("user?" + mlabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg":"Error obteniendo usuarios"
    }
    res.send(response);
  }
)
}
);

app.get('/apitechu/v1/users/:id',
function(req, res)
{
  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  console.log("GET /apitechu/v1/users/:id");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("user?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);

app.get('/apitechu/v1/email',
function(req, res)
{
  var email = '"' + req.header.email + '"';
  var query = 'q={"email" : ' + email +'  }';
  console.log('mail: ' + email)
  console.log("GET /apitechu/v1/email");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");
  console.log(query)
  httpClient.get("user?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);

app.get('/apitechu/v1/iban',
function(req, res)
{
  console.log("consulto");
  console.log(req.headers);
  var query = 'q={"iban" : "' + req.headers.iban +'" }';
  console.log(query)
  console.log("GET /apitechu/v1/iban");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");
  console.log(query)
  httpClient.get("account?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
         res.send(response);
       } else {
         if (body.length > 0) {
           response = body[0];
           console.log("paso con")
           res.status(200);
           res.send(response);
         } else {
           console.log("paso sin")
           response = {
             "sinCta" : true
           };
           res.status(200);
           res.send(response);
         }
       }
     }
)
}
);

app.get('/apitechu/v1/maxid',
function(req, res)
{
  var query = 'f={"id":1}&s={"id":-1}&l=1';
  console.log("GET /apitechu/v1/maxid");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");
  console.log(query)
  httpClient.get("user?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);

app.post('/apitechu/v1/altausu',
function(req, res)
{
// Tomo datos del body
 var maxId = 1;
 var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "gender": req.body.gender,
    "ip_address": req.body.ip_address,
    "password": req.body.password,
    "id": 0
 };

 var queryExist = 'q={"email" : "' + req.body.email +'"  }';
 console.log("POST /apitechu/v1/altausu");
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 console.log(queryExist)
 bcrypt.hash(req.body.password, 10, function(err, hash) {
    console.log(hash)
    console.log(req.body.password)
    newUser.password = hash
 });
 httpClient.get("user?" + queryExist + "&" + mlabAPIKey,
 function(errExist, resExist, bodyExist) {
      if (errExist) {
        response = {
          "msg" : "Error alta usuario."
        }
        res.status(500);
        res.send(response);

      } else {
        if (bodyExist.length > 0) {
          response = {
            "msg" : "Usuario ya existente."
          }
          res.send(response);
         } else {
                var query = 'f={"id":1}&s={"id":-1}&l=1';
//----------------
                httpClient.get("user?" + query + "&" + mlabAPIKey,
                function(err, resMLab, body) {
                if (err) {
                      response = {
                           "msg" : "Error de servidor"
                      }
                     res.status(500);
                     res.send(response);
                     } else {
                             this.maxId = body[0].id;
                             newUser.id = this.maxId + 1
                             console.log(newUser)
                             httpClient.post("user?" + mlabAPIKey ,newUser,
                             function(errPost, resPost, bodyPost){
                             if (errPost) {
                                 console.log("paso6")
                                 response = {
                                       "msg" : "Error en alta de usuario"
                                 }
                                res.status(500);
                                res.send(response);
                            } else {
                               console.log("paso7")
                               var mensaje = 'Alta correcta de usuario : ' + newUser.last_name + ', ' + newUser.first_name
                               response = {
                               "msg" : mensaje,
                               "idUsuario" : newUser.id,
                               "firstNameUsuario" : newUser.first_name,
                               "lastNameUsuario" : newUser.last_name
                                }
                               res.status(200);
                               res.send(response);
                         }
                         }
                         );
                }
              }
              );
      }
    }
  }
)
}
);

app.post('/apitechu/v1/altamov',
function(req, res)
{
// Tomo datos del body
 var maxId = 1;
 var newMovto = {
    "id_account": req.body.id_account,
    "date_operation": "01/01/1900",
    "type_movto": req.body.type_movto,
    "import": req.body.import,
    "observation": req.body.observation,
    "id": 0
 };
 var now = new Date();
 var formato=date.format(now, 'DD/MM/YYYY');
 newMovto.date_operation = formato;
 var query = 'q={"id_account": ' + req.body.id_account + '}&f={"id":1}&s={"id":-1}&l=1';
 console.log("GET /apitechu/v1/altamov");
 // Creo cliente: conexion a MLAB para accesos a db
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 httpClient.get("movtos?" + query + "&" + mlabAPIKey,
 function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error de servidor"
        }
        res.status(500);
        res.send(response);
      } else {
                if (body.length > 0)
                {
                   this.maxId = body[0].id;
                } else {
                  this.maxId = 0
                }
                newMovto.id = this.maxId + 1
                console.log(newMovto)
                httpClient.post("movtos?" + mlabAPIKey ,newMovto,
                function(errPost, resPost, bodyPost){
                          if (errPost) {
                            response = {
                              "msg" : "Error en altar"
                               }
                            res.status(500);
                            console.log(response)
                            res.send(response);
                          } else {
                                response = {
                                "msg" : "alta correcta.",
                                "idMovto" : newMovto.id
                              }
                              console.log(response)
                              res.send(response);
                          }
                      }
                    );

           }
      }
    )
    }
);

//--
app.post('/apitechu/v1/modsaldo',
function(req, res)
{
 var newSaldo = 0
 var query = 'q={"id": ' + req.body.id_account + '}';
 console.log("POST /apitechu/v1/modsaldo");

 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 console.log(query);
 httpClient.get("account?" + query + "&" + mlabAPIKey,
 function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error de servidor"
        }
        res.status(500);
        res.send(response);
      } else {
                 console.log(body[0].balance);
                 console.log(req.body.type_movto);
                 req.body.type_movto == 0 ? newSaldo = body[0].balance + req.body.import : newSaldo = body[0].balance - req.body.import
                 console.log(newSaldo)
                 if (newSaldo > 0) {
                        var putBody = '{"$set":{"balance": ' + newSaldo + '}}';
                        console.log(putBody)
                        httpClient.put("account?" + query + "&" + mlabAPIKey,
                             JSON.parse(putBody),
                             function(errPut, resPut, bodyPut){
                                 if (errPut) {
                                   response = {
                                     "msg" : "Error modificando saldo."
                                      }
                                   res.status(500);
                                   res.send(response);
                                 } else {
                                   response = {
                                       "msg" : "Saldo actualizado correctamente",
                                       "id" : body[0].id,
                                       "saldoAct" : newSaldo
                                     }
                                   res.send(response);
                                 }
                             }
                           );
                         } else {
                               response = {
                                 "msg" : "Saldo insuficiente."
                               }
                               res.status(404);
                               res.send(response);
                           }
           }
      }
    )
    }
);
//--

app.post('/apitechu/v1/login',
function(req, res)
{
// Tomo datos del body
 var newUser = {
    "email" : req.body.email,
    "password" : req.body.password
 };

// var query = 'q={"email" : "' + newUser.email + '" , "password": "' + newUser.password + '"}';
 var query = 'q={"email" : "' + newUser.email + '"}';
 console.log("GET /apitechu/v1/login");
 // Creo cliente: conexion a MLAB para accesos a db
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 httpClient.get("user?" + query + "&" + mlabAPIKey,
 function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0) {

            bcrypt.compare(req.body.password, body[0].password, function(errCrip, resCrip) {
             if(resCrip) {
              var queryPut = 'q={"id" : ' + body[0].id +'}';
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + queryPut + "&" + mlabAPIKey,
                   JSON.parse(putBody),
                   function(errPut, resPut, bodyPut){
                       if (errPut) {
                         response = {
                           "msg" : "Error haciendo login."
                            }
                         res.status(500);
                         res.send(response);
                       } else {
                         response = {
                             "msg" : "login correcto.",
                             "idUsuario" : body[0].id,
                             "firstNameUsuario" : body[0].first_name,
                             "lastNameUsuario" : body[0].last_name
                           }
                         res.send(response);
                       }
                   }
                 );

             } else {
              response = {
                "msg" : "Usuario/Password no encontrado."
              }
              res.status(404);
              res.send(response);
             }
            })
                  } else {
                        response = {
                          "msg" : "Usuario/Password no encontrado."
                        }
                        res.status(404);
                        res.send(response);
                    }
           }
      }
    )
    }
);


app.post('/apitechu/v1/logout',
function(req, res)
{

 var query = 'q={"id" : ' + req.body.id + ' , "logged": true}';
 console.log("GET /apitechu/v1/logout");
 // Creo cliente: conexion a MLAB para accesos a db
 httpClient = requestJson.createClient(baseMlabURL);
 console.log("cliente creado");
 httpClient.get("user?" + query + "&" + mlabAPIKey,
 function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0) {
                 var queryPut = 'q={"id" : ' + req.body.id +'}';
                 var putBody =  '{"$unset":{"logged":""}}';
                 httpClient.put("user?" + queryPut + "&" + mlabAPIKey,
                      JSON.parse(putBody),
                      function(errPut, resPut, bodyPut){
                          if (errPut) {
                            response = {
                              "msg" : "Error haciendo logout."
                               }
                            res.status(500);
                            res.send(response);
                          } else {
                            response = {
                                "msg" : "deslogado correcto.",
                                "idUsuario" : body[0].id,
                                "firstNameUsuario" : body[0].first_name,
                                "lastNameUsuario" : body[0].last_name
                              }
                            res.send(response);
                          }
                      }
                    );
                  } else {
                        response = {
                          "msg" : "Logout incorrecto"
                        }
                        res.status(404);
                        res.send(response);
                    }
           }
      }
    )
    }
);

app.get('/apitechu/v1/users/:id/account',
function(req, res)
{
  var user_id = req.params.id;
  var query = 'q={"user_id" : ' + user_id + '}';

  console.log("GET /apitechu/v1/users/:id/account");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("account?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);

app.get('/apitechu/v1/account/:id/movtos',
function(req, res)
{
  var id_account = req.params.id;
  var query = 'q={"id_account" : ' + id_account + '}';

  console.log("GET /apitechu/v1/account/:id/movtos");
  httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("movtos?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movtos."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           var movtos = body
           for (movto of movtos)
           {
             movto.type_movto == 0 ? movto.import = movto.import * 1 : movto.import = movto.import * (-1)
           }
           response = movtos;
         } else {
           response = {
             "msg" : "cuenta sin movtos."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
)
}
);
